package com.lara;

import org.apache.log4j.Logger;

public class Bootstrap 
{
	final static org.apache.log4j.Logger logger = Logger.getLogger(Bootstrap.class);
	public static void main(String[] args) 
	{
	    logger.trace("trace log");
	    logger.debug("debug log");
	    logger.info("info log");
	    logger.warn("warn log");
	    logger.error("error log");
	    logger.fatal("trace log");
	}
}
